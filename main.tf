provider "aws" {
  region	= "us-west-2"
  access_key	= "${var.access_key}"
  secret_key	= "${var.secret_key}"
}

resource "aws_instance" "build" {
  ami		= "ami-0475f60cdd8fd2120"
  instance_type	= "t2.nano"
  vpc_security_group_ids     = ["${aws_security_group.bld_prd_sg.id}"]
  subnet_id	= "${aws_subnet.bldprd_subnet.id}"
  key_name 	= "id_rsa"
  tags {
    Name = "build-env"
  }
}

resource "aws_instance" "production" {
  ami		= "ami-0475f60cdd8fd2120"
  instance_type = "t2.nano"
  key_name	= "id_rsa"
  connection {
    type = "ssh"
    user = "ubuntu"
    private_key = "${file("~/.ssh/id_rsa")}"
  }
  tags {
    Name = "production-env"
  }
}

resource "aws_key_pair" "build" {
  key_name = "id_rsa"
  public_key = "${file("~/.ssh/id_rsa.pub")}"
}

resource "aws_vpc" "buildprod_vpc" {
  cidr_block		= "10.0.0.0/16"
  enable_dns_hostnames	= true
  enable_dns_support	= true
  tags = {
    Name = "Build and Production VPC"
  }
}

resource "aws_eip" "eip_test" {
  instance	= "${aws_instance.build.id}"
  vpc		= true
}

resource "aws_subnet" "bldprd_subnet" {
  vpc_id	= "${aws_vpc.buildprod_vpc.id}"
  cidr_block	= "10.0.1.0/24"
  
  tags = {
    Name = "bldprd_sub"
  }
}

resource "aws_security_group" "bld_prd_sg" {
  name = "Bld-Prd Security Group"
  vpc_id = "${aws_vpc.buildprod_vpc.id}"
  
  ingress {
    from_port	= 22
    to_port	= 22
    protocol	= "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  egress {
    from_port 	= 0
    to_port	= 0
    protocol	= "-1"
    cidr_blocks	= ["0.0.0.0/0"]
  }
}

resource "aws_internet_gateway" "buildprod_gate" {
  vpc_id	= "${aws_vpc.buildprod_vpc.id}"

  tags {
    Name = "build-prod Gateway"
  }
}

resource "aws_route_table" "buildprod_rtable" {
  vpc_id	= "${aws_vpc.buildprod_vpc.id}"

  tags {
    Name = "build-prod Route Table"
  }
}

resource "aws_route_table_association" "subnet_association" {
  subnet_id		= "${aws_subnet.bldprd_subnet.id}"
  route_table_id	= "${aws_route_table.buildprod_rtable.id}"
}
