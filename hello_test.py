import unittest
from hello import hello_world

class TestHelloMethod(unittest.TestCase):

    def test_hello_world(self):
        str = "Hello World"
        self.assertEqual(hello_world(str), str)

if __name__ == '__main__':
    unittest.main()
